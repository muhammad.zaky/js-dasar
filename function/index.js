const karyawan = [

  {
    name: "zaky",
    lama_bekerja: 5,
    gaji_pokok: 1000000
  },

  {
    name: "kevin",
    lama_bekerja: 21,
    gaji_pokok: 70000000
  },

]

const bonus = 1000000;

function hitungBonus(karyawan, bonus) {
  if (typeof karyawan === 'object' && karyawan.name !== undefined) {
    const bonusKaryawan = karyawan.gaji_pokok + karyawan.lama_bekerja * bonus;
    console.log(`${karyawan.name}: ${bonusKaryawan}`);
  } else {
    console.log(`Data Salah`)
  }
}

karyawan.forEach((karyawanElement, index) => {
  hitungBonus(karyawanElement, bonus)
})

// for (i = 1; i <= 3; i++) {
//   hitungBonus(karyawan[i - 1], bonus)
// }

// function calculateBonusByName(name, bonus, karyawan) {
//   const dataKaryawan = karyawan.filter(item => item.name === name)[0];
//   const bonusKaryawan = dataKaryawan.lama_bekerja * bonus;
//   console.log(`${dataKaryawan.name}: ${bonusKaryawan}`);
// }

// calculateBonusByName('dian', bonus, karyawan);


// 1. gimana kalau kita gak tau nomor index tapi tau nama
// 2. gak tau index dan mau print semua bonus


// const bonusZaky = karyawan[0].lama_bekerja * bonus;
// const bonusKevin = karyawan[1].lama_bekerja * bonus;
// const bonusDian = karyawan[2].lama_bekerja * bonus;

// console.log(`Zaky: ${bonusZaky}`);
// console.log(`Kevin: ${bonusKevin}`);
// console.log(`dian: ${bonusDian}`);
