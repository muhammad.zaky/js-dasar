class Bonus {
  constructor(nominalBonus, kedua = 2) {
    this.nominalBonus = nominalBonus;
    this.kedua = kedua;
  }

  hitung(karyawan) {
    if (typeof karyawan === 'object' && karyawan.name !== undefined) {
      const bonusKaryawan = this.rumus(karyawan.gaji_pokok, karyawan.lama_bekerja)
      console.log(`Bonus ${karyawan.name}: ${bonusKaryawan}`);
    } else {
      console.log(`Data Salah`)
    }
  }

  hitungThr(karyawan) {
    const thrKaryawan = this.rumus(karyawan.gaji_pokok, karyawan.lama_bekerja);
    console.log(`THR ${karyawan.name}: ${thrKaryawan}`);
  }

  rumus(gajiPokok, lamaBekerja) {
    return this.nominalBonus * this.kedua;
  }
}

const karyawan = [
  {
    name: "zaky",
    lama_bekerja: 5,
    gaji_pokok: 1000000
  },
  {
    name: "kevin",
    lama_bekerja: 21,
    gaji_pokok: 70000000
  },
];
const nominalBonus = 500000;

const bonus = new Bonus(nominalBonus);

karyawan.forEach((karyawanElement, index) => {
  bonus.hitung(karyawanElement)
  bonus.hitungThr(karyawanElement)
})