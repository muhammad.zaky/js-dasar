let bgColor = document.getElementById('colorChanger');

bgColor.addEventListener("change", function (e) {
  document.body.style.backgroundColor = bgColor.value;
})

let textChanger = document.getElementById('textColorChanger');

textChanger.addEventListener("change", event => {
  document.body.style.color = textChanger.value;
})

let textAdder = document.getElementById('textAdder');
let h1 = document.getElementById("textH1")

textAdder.addEventListener("change", function (e) {
  h1.textContent = textAdder.value;
})

let deleteText = document.getElementById("deleteText");

deleteText.addEventListener("click", function (e) {
  h1.textContent = "";
  textAdder.value = "";
})

let angka = document.getElementById("angka");
let nilai = parseInt(angka.innerText);

let plusButton = document.getElementById("plusButton");
plusButton.addEventListener("click", function (e) {
  nilai++;
  angka.innerText = nilai;
})

let minusButton = document.getElementById("minusButton");
minusButton.addEventListener("click", function (e) {
  nilai--;
  angka.innerText = nilai;
})

let resetButton = document.getElementById("resetButton");
resetButton.addEventListener("click", function (e) {
  nilai = 0;
  angka.innerText = nilai;
})