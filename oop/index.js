class Karyawan {
  constructor(name, nik, salary) {
    if (this.constructor === Karyawan) {
      throw new Error('Class tidak boleh di gunakan');
    }
    this.name = name;
    this.nik = nik;
    this.salary = salary;

    if (this.constructor.name === 'Programmer' || this.constructor.name === 'Fullstack') {
      this.bahasaPemograman = 'JS';
    }
  }

  introduce() {
    console.log(`hi, saya ${this.name}`);
  }

  gajian() {
    console.log(`gaji ${this.name} adalah ${this.salary}`)
  }

  bonus() {
    const bonus = this.#hitungBonus();
    console.log(`bonus ${this.name} = ${bonus} `);
  }

  #hitungBonus() {
    return this.salary * 4;
  }

  _kehadiran() {
    console.log('hadir');
  }

  _bahasaPemograman() {
    return this.bahasaPemograman;
  }
}

class Programmer extends Karyawan {
  constructor(name, nik, salary) {
    super(name, nik, salary);
  }

  introduce() {
    super.introduce();
    console.log('saya bisa ngoding');
  }

  ngoding() {
    console.log(`${this.name} lagi ngoding ${this._bahasaPemograman()}`);
  }

  cekKehadiran() {
    super._kehadiran();
  }
}

class Fullstack extends Programmer {
  constructor(name, nik, salary) {
    super(name, nik, salary);
  }

  introduce() {
    super.introduce();
    console.log('saya fullstack');
  }
}

class Intern extends Karyawan {
  constructor(name, nik, salary) {
    super(name, nik, salary);
  }

  gajian() {
    console.log(`${this.name} anak magang, jadi tidak dapat gaji`);
  }
}

const karyawan = [
  {
    name: "zaky",
    nik: 511111,
    gaji_pokok: 1000000,
    jabatan: 'karyawan'
  },
  {
    name: "kevin",
    nik: 21123,
    gaji_pokok: 70000000,
    jabatan: 'programmer'
  },
  {
    name: "rauf",
    nik: 511111,
    gaji_pokok: 1000000,
    jabatan: 'fullstack'
  },
  {
    name: "pandu",
    nik: 21123,
    gaji_pokok: 70000000,
    jabatan: 'intern'
  },
  {
    name: "nabhan",
    nik: 511111,
    gaji_pokok: 1000000,
    jabatan: 'programmer'
  },
  {
    name: "naufal",
    nik: 21123,
    gaji_pokok: 70000000,
    jabatan: 'hrd'
  },
]

// const programmer = karyawan.filter(item => item.jabatan === 'programmer');
// programmer.forEach((item) => {
//   const karywanProgrammer = new Programmer(item.name, item.nik, item.gaji_pokok);
//   karywanProgrammer.introduce();
//   karywanProgrammer.ngoding();
// })

const andi = new Intern('andi', 123456, 1000000);
const zaky = new Programmer('zaky', 11111, 20000000);
const kevin = new Intern('kevin', 1234, 500000);
const rauf = new Fullstack('rauf', 191919, 20000000);


andi.introduce();
andi.gajian();
andi.bonus();

kevin.introduce();
kevin.gajian();
kevin.bonus();

zaky.introduce();
zaky.gajian();
zaky.bonus();
zaky.ngoding();

rauf.introduce();
rauf.gajian();
rauf.bonus();
rauf.ngoding();