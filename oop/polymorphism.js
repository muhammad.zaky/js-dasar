class Karyawan {
  constructor(name, nik, salary) {
    this.name = name;
    this.nik = nik;
    this.salary = salary;
  }

  introduce() {
    console.log(`hi, saya ${this.name}`);
  }

  gajian() {
    console.log(`gaji ${this.name} adalah ${this.salary}`)
  }

  bekerja() {
    console.log('menggunakan buku dan pulpen');
  }
}

const Programmer = Base => class extends Base {
  ngoding() {
    console.log(`${this.name} lagi ngoding JS`);
  }
}

const Fullstack = Base => class extends Base {
  webDesing() {
    console.log('fullstack melakukan web design');
  }
}

class BackendEngineer extends Programmer(Karyawan) {
  constructor(props) {
    super(props)
  }

  bekerja() {
    super.bekerja();
    super.ngoding();
  }
}

class FullstackEngineer extends Programmer(Fullstack(Karyawan)) {
  constructor(props) {
    super(props)
  }

  bekerja() {
    super.bekerja();
    super.ngoding();
    super.webDesing();
  }
}

class WebDesing extends Fullstack(Karyawan) {
  constructor(props) {
    super(props)
  }

  bekerja() {
    super.bekerja();
    super.webDesing();
  }
}

const zaky = new BackendEngineer('zaky', 123123123, 20000000);
console.log('1.zaky')
zaky.bekerja();

const dian = new Karyawan('dian', 11111, 10000000);
console.log('2.dian')
dian.bekerja();

const kevin = new FullstackEngineer('kevin', 121212, 25000000);
console.log('3. kevin');
kevin.bekerja();

const sinung = new WebDesing('sinung', 1313131, 12000000);
console.log('4. sinung');
sinung.bekerja();